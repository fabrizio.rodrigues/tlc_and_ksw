// provision apigateway resource instance
resource "ibm_resource_instance" "apigateway" {
  name     = var.service_name
  location = "global"
  service  = "api-gateway"
  plan     = "lite"
}

// provision endpoint resources for a directory of api douments..
resource "ibm_api_gateway_endpoint" "routeFmsApi" {
  service_instance_crn = ibm_resource_instance.apigateway.id
  managed              = var.managed
  name                 = "routeFmsApi"
  open_api_doc_name    = "./apigw.json"
  //type                 = var.action_type //required only when updating action
}





