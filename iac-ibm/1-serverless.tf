terraform {
  required_providers {
    ibm = {
      source  = "IBM-Cloud/ibm"
      version = ">= 1.26.0"
    }
  }
}
provider "ibm" {
  region = var.region
}

