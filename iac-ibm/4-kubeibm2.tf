resource "ibm_container_cluster" "testacc_cluster" {
  name            = "my_cluster_lab"
  datacenter      = "dal10"
  hardware        = "shared"
  machine_type    = "free"
}

//ibmcloud iam orgs --guid para obter o id 
data "ibm_container_cluster_config" "cluster_config" {
  cluster_name_id = ibm_container_cluster.testacc_cluster.name
}

resource "null_resource" "create_cluster_img" {
  provisioner "local-exec" {
    command = <<EOF
        export KUBECONFIG="${data.ibm_container_cluster_config.cluster_config.config_file_path}"
        kubectl config current-context
        kubectl create deployment python-docker-v3-deployment --image=us.icr.io/${ibm_cr_namespace.rg_namespace.name}/python-docker-v2:latest
        kubectl expose deployment/python-docker-v3-deployment --type=NodePort --port=5000 --name=python-docker-v3-service --target-port=5000
        kubectl describe service python-docker-v3-service
        ibmcloud ks worker ls --cluster ${ibm_container_cluster.testacc_cluster.name}
        
EOF

  }
  depends_on = [ibm_container_cluster.testacc_cluster]
}
