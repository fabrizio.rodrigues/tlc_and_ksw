variable "region" {
  description = "Define what region the instance will be deployed"
  default     = "us-south"
  # "us-south/br-sao"
}

variable "service_name" {
  type        = string
  description = "Service instance name"
  default = "API-Terraform"
}

variable "managed" {
  type        = string
  description = "Indicates if endpoint to be online or offline"
  default     = "true"
}
variable "action_type" {
  type        = string
  description = "Update actions manage,unmanage,share,unshare"
  default     = "unshare"
}

variable "subscription_name" {
  type        = string
  description = "Subscription resource name"
  default = "apigw-key"
}
variable "subscription_type" {
  type        = string
  description = "Subscription type internal, external"
  default = "external"
}
variable "secret" {
  type        = bool
  description = "Client secret of subscription"
  default = false
}

variable "cr_namespace_name" {
  type = string
  default = "bff-terra-erp"
}
