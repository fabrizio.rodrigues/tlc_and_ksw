data "ibm_resource_group" "resource_group" {
  name = "Default"
}

resource "ibm_cr_namespace" "rg_namespace" {
  name              = var.cr_namespace_name
  resource_group_id = data.ibm_resource_group.resource_group.id
}

resource "null_resource" "create_build_image" {
  provisioner "local-exec" {
# depreciado  => command = "ibmcloud cr build --accept-deprecation -t br.icr.io/${ibm_cr_namespace.rg_namespace.name}/python-docker-v2 /Users/fabrizio.rodriguesibm.com/Documents/cloudArchitect/python/test1/imagem/backforfront-api-generic/."  
    command = <<EOF
        docker build -t us.icr.io/${ibm_cr_namespace.rg_namespace.name}/python-docker-v2 /home/Fabrizio_Rodrigues/terralab/.
        ibmcloud login --apikey <<coloque sua apikey>>
        ibmcloud target -g Default
        ibmcloud cr login
        docker push us.icr.io/${ibm_cr_namespace.rg_namespace.name}/python-docker-v2
    EOF
  }
}

# ibmcloud cr region
# ibmcloud regions br-sao
#O seu destino é a região 'br-sao', o registro é 'br.icr.io'.
# ibmcloud cr region-set us-south
#A região está configurada como 'us-south', o registro é 'us.icr.io'.
#
