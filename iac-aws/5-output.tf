output "curl_stage_invoke_url" {
  description = "API Gateway Stage Invoke uRL"
  value       = "curl ${aws_api_gateway_stage.routefmsapis.invoke_url}${var.rest_api_path}"
}

output "curl_stage_2_invoke_url" {
  description = "Second API Gateway Stage Invoke uRL"
  value       = "curl ${aws_api_gateway_stage.routefmsapis.invoke_url}${var.rest_api_2_path}"
}