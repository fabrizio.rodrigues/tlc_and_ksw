variable "region" {
  description = "Define what region the instance will be deployed"
  default     = "sa-east-1"
}

variable "rest_api_path" {
  default     = "/poc-routeapi/{proxy+}"
  description = "Path to create in the API gateway"
  type        = string
}

variable "rest_api_2_path" {
  default     = "/poc-routeapi-service"
  description = "Path to create in the API gateway the service backend"
  type        = string
}