resource "aws_api_gateway_rest_api" "routeFmsApi" {
  name = "routeFmsApi"
  body = jsonencode({
    openapi = "3.0.1"
    info = {
      title   = "API para demonstrar o acesso via proxy"
      version = "1.0"
    }
    #x-amazon-apigateway-request-validators = {
    #  all = {
    #    requestParameters = {
    #        "method.request.path.proxy" = true
    #    }
    #  }
    #}
    paths = {
      (var.rest_api_path) = {
        get = {
          parameters = [
              {
                "in" = "path"
                "name" = "proxy"
                "required" = true
              },
              {
                "in" = "query"
                "name" = "priceList"
                "required" = true
              },
              {
                "in" = "query"
                "name" = "term"
                "required" = true
              },
              {
                "in" = "query"
                "name" = "page"
                "required" = true
              },
              {
                "in" = "query"
                "name" = "resultsPerPage"
                "required" = true
              }
          ]
          x-amazon-apigateway-integration = {
            httpMethod           = "GET"
            payloadFormatVersion = "1.0"
            type                 = "HTTP_PROXY"
            uri                  = "http://169.57.43.240:32511/{proxy}"
            requestParameters = {
              "integration.request.path.proxy" = "method.request.path.proxy"
              "integration.request.querystring.priceList": "method.request.querystring.priceList"
              "integration.request.querystring.term": "method.request.querystring.term"
              "integration.request.querystring.page": "method.request.querystring.page"
              "integration.request.querystring.resultsPerPage": "method.request.querystring.resultsPerPage"
            }
          }
        }
        post = {
          parameters = [{
            "in" = "path"
            "name" = "proxy"
            "required" = true
          }]
          x-amazon-apigateway-integration = {
            httpMethod           = "POST"
            payloadFormatVersion = "1.0"
            type                 = "HTTP_PROXY"
            uri                  = "http://169.57.43.240:32511/{proxy}"
            requestParameters = {
              "integration.request.path.proxy" = "method.request.path.proxy"
            }
          }
        }
        options = {
          responses = {
            200 = {
              description = "exemplo"
            }
          }
          x-amazon-apigateway-integration = {
            httpMethod = "OPTIONS"
            type       = "MOCK"
            responses = {
              200 = {
                statusCode = 200
                response_templates = {
                  "application/json" = ""
                }
                response_parameters = {
                  "method.response.header.Access-Control-Allow-Origin"  = "'*'",
                  "method.response.header.Access-Control-Allow-Headers" = "'*,origin, x-requested-with, accept, authorization, WCTrustedToken, WCToken, content-type, checkout-api-version, storeId, channel, ip, userAgent'",
                  "method.response.header.Access-Control-Allow-Methods" = "'GET, PUT, POST, DELETE'",
                  "method.response.header.Access-Control-Max-Age"       = "'3628800'",
                  "method.response.header.Accept"                       = "'*/*'",
                  "method.response.header.Referrer-Policy"              = "'no-referrer-when-downgrade, no-referrer, strict-origin, origin, same-origin, origin-when-cross-origin, unsafe-url'"
                }
              }
            }
            requestTemplates = {
              "application/json" = ""
            }
          }
        }
      }
      (var.rest_api_2_path) = {
        get = {
          x-amazon-apigateway-integration = {
            httpMethod           = "GET"
            payloadFormatVersion = "1.0"
            type                 = "HTTP_PROXY"
            uri                  = "http://169.57.43.240:32511"
            requestParameters = {
              "integration.request.header.mpc-api-version" = "'v1'"
              "integration.request.header.token"           = "'89DB0D3A-53E7-41ED-8449-0D9AFD4A3D11'"
            }
          }
        }
      }
    }


  })

  endpoint_configuration {
    types = ["REGIONAL"]
  }
}


resource "aws_api_gateway_deployment" "routefmsapis" {
  rest_api_id = aws_api_gateway_rest_api.routeFmsApi.id
  triggers = {
    "redeployment" = sha1(jsonencode(
      aws_api_gateway_rest_api.routeFmsApi.body
    ))
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_stage" "routefmsapis" {
  deployment_id = aws_api_gateway_deployment.routefmsapis.id
  rest_api_id   = aws_api_gateway_rest_api.routeFmsApi.id
  stage_name    = "dev"
}
